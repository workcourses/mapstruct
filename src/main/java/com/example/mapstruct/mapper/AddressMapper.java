package com.example.mapstruct.mapper;

import com.example.mapstruct.dto.AddressDTO;
import com.example.mapstruct.model.Address;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface AddressMapper {
    @Mappings({
            @Mapping(target = "streetName", source = "address.name"),
            @Mapping(target = "streetNumber", source = "address.number")
    })
    AddressDTO addressToDTO(Address address);

    @Mappings({
            @Mapping(target = "name", source = "dto.streetName"),
            @Mapping(target = "number", source = "dto.streetNumber")
    })
    Address dtoToAddress(AddressDTO dto);
}
