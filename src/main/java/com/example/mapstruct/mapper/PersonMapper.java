package com.example.mapstruct.mapper;

import com.example.mapstruct.dto.PersonDTO;
import com.example.mapstruct.model.Address;
import com.example.mapstruct.model.Person;
import com.example.mapstruct.repository.AddressRepository;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring"/*, uses = {PersonMapper.class}*/)
public abstract class PersonMapper {

    @Autowired
    private AddressRepository addressRepository;

    @Mappings({
            @Mapping(target = "firstName", source = "person.firstName"),
            @Mapping(target = "lastName", source = "person.lastName"),
            @Mapping(target = "address", ignore = true)
    })
    public abstract PersonDTO personToDTO(Person person);

    @Mappings({
            @Mapping(target = "firstName", source = "dto.firstName"),
            @Mapping(target = "lastName", source = "dto.lastName"),
            @Mapping(target = "address", ignore = true)
    })
    public abstract Person dtoToPerson(PersonDTO dto);

    @Mappings({
            @Mapping(target = "firstName", source = "person.firstName"),
            @Mapping(target = "lastName", source = "person.lastName"),
            @Mapping(target = "address", source = "address.city")
    })
    public abstract PersonDTO personToDTOV2(Person person, Address address);

    @BeforeMapping
    public void beforeMapping(Person person, @MappingTarget PersonDTO dto){
        StringBuilder stringBuilder = new StringBuilder();
        Address address = person.getAddress();
        stringBuilder.append(address.getCity());
        stringBuilder.append(" , ");
        stringBuilder.append(address.getName());
        stringBuilder.append(" , ");
        stringBuilder.append(address.getNumber().toString());
        dto.setAddress(stringBuilder.toString());
    }

    @BeforeMapping
    public void beforeMapping(PersonDTO dto, @MappingTarget Person person){
        String address = dto.getAddress();
        System.out.println(address);
        person.setAddress(new Address());
    }
//    can be user for post processing
//    @AfterMapping
//    public void afterMapping(@MappingTarget PersonDTO dto){
//
//    }
//    mapper function that can be used to do custom mapping between fields
//    public String map(Address address){
//        return address.getCity();
//    }
}
