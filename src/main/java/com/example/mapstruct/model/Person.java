package com.example.mapstruct.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Person {
    @Id
    @Column(name = "id_person")
    Long id;
    @Column(name = "first_name")
    String firstName;
    @Column(name = "last_name")
    String lastName;
    @JoinColumn(name = "id_address")
    @ManyToOne
    Address address;
}
