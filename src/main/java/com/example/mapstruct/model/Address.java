package com.example.mapstruct.model;

import lombok.Data;
import org.springframework.context.annotation.Primary;

import javax.persistence.*;

@Data
@Entity
public class Address {
    @Id
    @Column(name = "id_address")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Long id;
    @Column(name = "street_name", length = 255)
    String name;
    @Column(name = "street_number")
    Long number;
    @Column(length = 255)
    String city;
}
