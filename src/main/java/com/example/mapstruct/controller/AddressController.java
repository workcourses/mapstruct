package com.example.mapstruct.controller;

import com.example.mapstruct.dto.AddressDTO;
import com.example.mapstruct.mapper.AddressMapper;
import com.example.mapstruct.model.Address;
import com.example.mapstruct.repository.AddressRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping("/address")
public class AddressController {

    private final AddressRepository addressRepository;
    private final AddressMapper addressMapper;

    @GetMapping()
    public List<AddressDTO>list(){
        return addressRepository.findAll().stream().map(addressMapper::addressToDTO).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public AddressDTO find(@PathVariable("id") Long id){
        return addressRepository.findById(id).map(addressMapper::addressToDTO)
                .orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));
    }

    @PostMapping
    public ResponseEntity add(@RequestBody AddressDTO dto){
        Address address = addressMapper.dtoToAddress(dto);
        Address saved = addressRepository.save(address);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(saved.getId())
                .toUri();
        return ResponseEntity.created(location).body(saved);
    }
}
