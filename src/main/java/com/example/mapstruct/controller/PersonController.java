package com.example.mapstruct.controller;

import com.example.mapstruct.dto.PersonDTO;
import com.example.mapstruct.mapper.PersonMapper;
import com.example.mapstruct.model.Person;
import com.example.mapstruct.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/person")
@RequiredArgsConstructor
public class PersonController {

    private final PersonRepository personRepository;
    private final PersonMapper personMapper;

    @GetMapping
    public List<PersonDTO> list(){
        return personRepository.findAll().stream().map(personMapper::personToDTO).collect(Collectors.toList());
    }

    @GetMapping("/v2")
    public List<PersonDTO> listV2(){
        return personRepository.findAll().stream().map(p->personMapper.personToDTOV2(p,p.getAddress())).collect(Collectors.toList());
    }

    @PostMapping
    public ResponseEntity test(@RequestBody PersonDTO personDTO){
        Person person = personMapper.dtoToPerson(personDTO);
        System.out.println(person.toString());
        return ResponseEntity.ok().build();
    }
}
