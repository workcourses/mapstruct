package com.example.mapstruct.dto;

import lombok.Data;

@Data
public class PersonDTO {
    String firstName;
    String lastName;
    String address;
}
