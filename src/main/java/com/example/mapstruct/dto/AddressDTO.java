package com.example.mapstruct.dto;

import lombok.Data;

@Data
public class AddressDTO {
    private String streetName;
    private Long streetNumber;
    private String city;
}
