create table address(
    id_address bigint auto_increment,
    street_name varchar(255),
    street_number bigint,
    city varchar(255),
    primary key(id_address)
);

create table person(
    id_person bigint auto_increment,
    first_name varchar(255),
    last_name varchar(255),
    id_address bigint,
    primary key(id_person),
    foreign key(id_address) references address(id_address)
)